<!DOCTYPE html> <html lang="ru"> <head>
  <link rel="stylesheet"
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script
			src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
    <title> Форма Жмурко </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width= device-width, initial-scale=1.0">
  </head>
  <body>
    <div class="px-0 px-sm-3 container">
</div>
<?php 
  if(!empty($response_message)) {
    print "<div>";
    print $response_message;
    print "</div>";
  }
?>
<?php if(empty($_SESSION['login']))
      print "<div style ='text-align: center; '><h6>Вы можете <a id=\"buttonLogIn\" href='login.php'>войти</a> с логином и паролем для изменения данных или как <a id=\"buttonLogIn\" href='admin.php'>админ</a></h6></div>" ?></h6></div>" ?>
    <?php if(!empty($_SESSION['login'])) 
      print "<div style ='text-align: center;'><h6><a id=\"buttonLogIn\" href='login.php'>Выйти из аккаунта</a></h6></div>" ?>



  <div class="form container col-12 mt-3 mb-0">
    <section id="form"> <h2>Форма </h2>
  <form action="."
    method="POST">
    <label>
      Имя<br />
      <input name="name"
      <?php if($_COOKIE['name_error']) print "class=error" ?>
        value='<?php print "{$name_field_value}" ?>'/>
    </label><br />
<br/>
    <label>
      E-mail:<br />
      <input name="email"
      <?php if($_COOKIE['email_error']) print "class=error" ?>
        value='<?php print "{$email_field_value}" ?>'
        type="email" />
    </label><br />
<br/>
    <label>
      Дата рождения:<br />
      <input name="birthday"
      <?php if($_COOKIE['birthday_error']) print "class=error" ?>
        value='<?php print "{$bd_field_value}" ?>'
        type="date" />
    </label><br />
<br/>
    Пол:<br />
    <label><input type="radio" <?php if(is_null($_COOKIE['gender_last_value']) or !empty($_COOKIE['gender_error'])) print "checked" ?> name="gender" value="M" 
              <?php if($_COOKIE['gender_last_value'] == 'M') print "checked" ?> />
      мужской
    </label>
    <label><input type="radio"
      name="gender" value="F" 
	  <?php if($_COOKIE['gender_last_value'] == 'F') print "checked" ?>/>

      женский
    </label>
    <br/>
    <br/>
    Количество конечностей:<br />
    <label><input type="radio"
    name="limbs" value="1" <?php if($_COOKIE['limb_number_last_value'] == 1) print "checked" ?>/>
      1
    </label>

    <label><input type="radio"
      name="limbs" value="2" <?php if($_COOKIE['limb_number_last_value'] == 2) print "checked" ?>/>
      2
    </label>

    <label><input type="radio"
      name="limbs" value="3" <?php if($_COOKIE['limb_number_last_value'] == 3) print "checked" ?>/>
      3
    </label> 

    <label><input type="radio"
      name="limbs" value="4" <?php if($_COOKIE['limb_number_last_value'] == 4) print "checked" ?>/>
      4
    </label>
    <label>
    	<input type="radio"  <?php if(is_null($_COOKIE['limb_number_last_value']) or !empty($_COOKIE['limb_number_error'])) print "checked" ?> name="limbs" value="5"  
              <?php if($_COOKIE['limb_number_last_value'] == 5) print "checked" ?>
/>
    	8+
    </label>
<br/>
    <br />
    <label>
      Сверхспособности:
      <br />
      <select name="abilities[]"
        multiple="multiple">
        <option value="1">Бессмертие</option>
        <option value="2" selected="selected">Прохождение сквозь стены</option>
         <option value="3" selected="selected">Невидимость</option>
        <option value="4"> Левитация</option>
        <option value="5" selected="selected">Мгновенная телепортация</option>
      </select>
    </label><br />
<br/>
    <label>
      Биография:<br />
       <textarea name="biography"><?php print "{$biography_field_value}" ?> Послушайте - когда-то, две жены тому назад, двести пятьдесят тысяч сигарет тому назад, три тысячи литров спиртного тому назад...Тогда, когда все были молоды...</textarea>
    </label><br />

    <br />
    <p> <label><input type="checkbox" checked="checked"
      name="contract" <?php if($_COOKIE['name_error']) print "class=error" ?>/>
      С контрактом ознакомлен</label></p>
      <br/>
    <a id= "submit_button"></a>
    <input type="submit" value="Отправить" />
  </form>
</section>
</div>
    </div>
    </div>
  </body>
</html>
